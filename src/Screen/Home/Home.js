import React from "react";
import FirstBlock from "./Blocks/FirstBlock/FirstBlock";
import SecondBlock from "./Blocks/SecondBlock/SecondBlock";
import ThirdBlock from "./Blocks/ThirdBlock/ThirdBlock";
import FourthBlock from "./Blocks/FourthBlock/FourthBlock";


function Home() {
  return (
    <div>
      <FirstBlock/>
      <SecondBlock/>
      <ThirdBlock/>
      <FourthBlock/>
    </div>
  );
}

export default Home;
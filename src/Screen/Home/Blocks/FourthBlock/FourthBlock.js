import React from "react";
import {health} from "./data/health";
import classes from './FourthBlock.module.css';
import image1 from './data/image1.png';
import image2 from './data/image2.png';
import rectangle1 from './data/rectangle1.png';
import rectangle2 from './data/rectangle2.png';
import rectangle3 from './data/rectangle3.png';
import rectangle4 from './data/rectangle4.png';

function FourthBlock() {
  return (
    <div>
      <div className={classes.text}>
        <div className={classes.title}>
          <p className={classes.titleRed}>Healthcare </p>
          <p>at your Fingertips.</p>
        </div>
        <p className={classes.subtitle}>
          Bringing premium healthcare features to your fingertips. User friendly mobile platform to</p>
        <p className={classes.subtitle}>
          bring healthcare to your fingertips. Signup and be a part of the new health culture.</p>
      </div>


      <div className={classes.rectangles}>
        <img src={rectangle1} className={classes.rectangle1}/>
        <img src={rectangle2} className={classes.rectangle2}/>
        <img src={rectangle3} className={classes.rectangle3}/>
        <img src={rectangle4} className={classes.rectangle4}/>
      </div>
        <div className={classes.icons} >
          {health.map((item) => {
            return (
            <div key={item.id} className={classes.icon} >
              <div>
                <item.icon className={classes.iconSmall}/>
              </div>
                <p className={classes.title1}>{item.title}</p>
                <p className={classes.subtitle1}>{item.subtitle}</p>
            </div>
            )
          })}
        </div>

      <div className={classes.social}>
        <a href="https://play.google.com/store/movies" target="_blank">
          <img src={image1} className={classes.google} />
        </a>
        <a href="https://www.apple.com/app-store/" target="_blank">
          <img src={image2}/>
        </a>
      </div>
    </div>
  );
}

export default FourthBlock;
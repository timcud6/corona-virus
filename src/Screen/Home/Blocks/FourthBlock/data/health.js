 import React from "react";
 import { MdEmojiPeople} from 'react-icons/md';
 import { GiHealing} from 'react-icons/gi';
 import { FaHeartbeat} from 'react-icons/fa';


export const health = [
   {
     id:1,
     icon: MdEmojiPeople,
     title: "Symptom Checker",
     subtitle: "Check if you are infected by COVID-19 with our Symptom Checker",
   },
   {
     id:2,
     icon: GiHealing,
     title: "24x7 Medical support",
     subtitle: "Consult with 10,000+ health workers about your concerns.",
   },
   {
     id:3,
     icon: FaHeartbeat,
     title: "Conditions",
     subtitle: "Bringing premium healthcare features to your fingertips.",
   }
 ];
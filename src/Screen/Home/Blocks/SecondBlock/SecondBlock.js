import React from "react";
import classes from './SecondBlock.module.css';
import rectangle from './data/rectangle.png';
import rectanglesmall from './data/rectanglesmall.png';
import image1 from './data/image1.png';

function SecondBlock() {
  return (
    <div className={classes.block}>

      <div>
        <div className={classes.rectangle}>
          <img src={rectangle}/>
        </div>
        <div className={classes.rectangle1}>
          <img src={rectanglesmall}/>
        </div>
        <div className={classes.rectangle2}>
          <img src={rectanglesmall}/>
        </div>
        <div className={classes.image}>
          <img src={image1}/>
        </div>
      </div>


      <div className={classes.text}>
        <div >
          <div className={classes.title}>
            <p>Stay safe with </p>
          </div>
          <div className={classes.title}>
            <p>Go</p>
              <p className={classes.titleRed}>Corona.</p>
          </div>
        </div>

        <div className={classes.subtitle}>
          <p>24x7 Support and user friendly mobile platform to bring</p>
           <p> healthcare to your fingertips. Signup and be a part </p>
           <p> of the new health culture.</p>
        </div>

        <div>
          <button className={classes.button}>Features</button>
        </div>
      </div>
    </div>
  );
}

export default SecondBlock;
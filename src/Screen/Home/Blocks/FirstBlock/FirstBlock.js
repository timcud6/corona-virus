import React from "react";
import Navbar from "./Navbar";
import classes from './FirstBlock.module.css';
import image from './date/image.png';
import background from './date/background.png';
import playvideo from './date/playvideo.png';

function FirstBlock() {
  return (
    <div>
      <div className={classes.background}>
        <img src={background}/>
      </div>
      <div className={classes.firstBlock}>
        <Navbar/>
        <div className={classes.block}>
          <div>
            <div className={classes.text}>

              <div className={classes.title}>
                <p>Take care of yours </p>
              </div>
              <div className={classes.title}>
                <p>family’s</p>
                <p className={classes.titleBlue}>health.</p>
              </div>
            </div>
            <div className={classes.subtitle}>
              <p>All in one destination for COVID-19 health queries.</p>
              <p> Consult 10,000+ health workers about your concerns.</p>
            </div>
            <div>
              <button className={classes.buttonRed}>GET STARTED</button>
            </div>
          </div>

          <div className={classes.image}>
            <img src={image} className={classes.imageSmall}/>
          </div>
        </div>

        <div className={classes.play}>
          <div className={classes.playVideos}>
            <img src={playvideo}/>
          </div>
          <div className={classes.playVideo}>
            <p className={classes.videoTitle}>Stay safe with GoCorona</p>
            <p className={classes.video}>Watch Video</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FirstBlock;
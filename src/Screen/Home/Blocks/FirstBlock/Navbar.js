import React from "react";
import classes from './FirstBlock.module.css';
import {FaVirus} from 'react-icons/fa';

function Navbar() {
  return (
    <div className={classes.navbar}>
      <div className={classes.logo}>
        <FaVirus className={classes.icon}/>
        <p className={classes.go}>Go</p>
        <p className={classes.corona}>Corona</p>
      </div>
      <div className={classes.links}>
        <a className={classes.link}>HOME</a>
        <a className={classes.link}>FEATURES</a>
        <a className={classes.link}>SUPPORT</a>
        <a className={classes.link}>CONTACT US</a>
      </div>
      <div>
        <button className={classes.button}>DOWNLOAD</button>
      </div>
    </div>
  );
}

export default Navbar;
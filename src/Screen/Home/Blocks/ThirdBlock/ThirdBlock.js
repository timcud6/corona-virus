import React from "react";
import classes from './ThirdBlock.module.css';
import rectangle01 from './data/rectangle01.png';
import rectangle02 from './data/rectangle02.png';
import rectangle03 from './data/rectangle03.png';
import ReactPlayer from "react-player";


function ThirdBlock() {

  return (
    <div className={classes.body}>
      <div className={classes.rectangles}>
        <img src={rectangle01} className={classes.image1}/>
        <img src={rectangle02} className={classes.image2}/>
        <img src={rectangle03} className={classes.image3}/>
      </div>
      <div className={classes.news}>
        <div className={classes.columnDifferent1}>
          <p className={classes.columnTitle}>2m</p>
          <p className={classes.columnSubtitle}>USERS</p>
        </div>
        <div className={classes.column}>
          <p className={classes.columnTitle}>78</p>
          <p className={classes.columnSubtitle}>COUNTRIES</p>
        </div>
        <div className={classes.columnDifferent2}>
          <p className={classes.columnTitle}>10,000+</p>
          <p className={classes.columnSubtitle}>MEDICAL EXPERTS</p>
        </div>
      </div>

      <div className={classes.secondBlock}>
        <div className={classes.text}>
          <div className={classes.title}>
            <p>Talk to </p>
            <p className={classes.titleBlue}>experts.</p>
          </div>
          <p className={classes.subtitle}>Book appointments or submit queries into thousands
            of forums concerning health issues and prevention against noval Corona Virus.</p>
          <button className={classes.button}>Features</button>
        </div>

        <div className={classes.video}>
          <ReactPlayer  url="https://www.youtube.com/watch?v=D9OOXCu5XMg&t=20s" className={classes.video2}/>
        </div>
      </div>
    </div>
  );
}

export default ThirdBlock;